package nl.munlock.irods;

import nl.munlock.graphdb.options.CommandOptions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.event.base.NotifyingRepositoryConnectionWrapper;
import org.eclipse.rdf4j.repository.event.base.RepositoryConnectionListenerAdapter;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static nl.munlock.irods.Download.downloadFile;
import static org.eclipse.rdf4j.model.util.Values.iri;

public class Load {
    static Logger logger = Logger.getLogger(Load.class);
    private static RepositoryConnection repositoryConnection;
    private static Repository repository;
    private static RemoteRepositoryManager remoteRepositoryManager;

    public static void hdt(CommandOptions commandOptions, HashMap<String, String> hdtFiles) throws IOException, JargonException {
        logger.info("Loading RDF files");

        makeConnection(commandOptions);

        // Obtain all hashes
        IRI predicate = iri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
        IRI object = iri("http://m-unlock.nl/ontology/RDFDataSet");
        RepositoryResult<Statement> repositoryResults = repositoryConnection.getStatements(null, predicate, object);
        HashSet<String> hashes = new HashSet<>();
        for (Statement repositoryResult : repositoryResults) {
            hashes.add(repositoryResult.getSubject().stringValue());
        }


        // iRODS connection for inputstream
        Connection connection = new Connection();
        int counter = 0;
        for (String hdtFile : hdtFiles.keySet()) {
            counter++;
            // Downloading the hdt file
            downloadFile(connection, new File(hdtFile));
            // Downloading the index file
            downloadFile(connection, new File(hdtFile + ".index.v1-1"));

            // Perform the splitter and split check...
            // First check if .nt.gz.0 exists... if so collect all files containing <name>.nt.gz.[0-1].*
            // TODO if < 10.000.000 no split needed perhaps rename to .nt.gz.0 and be done with it?
            File ntTriples = new File("." + hdtFile + ".nt.gz");
            // No conversion needed
            HashSet<File> localTripleFileSubsets = new HashSet<>();
            if (ntTriples.exists()) {
                logger.info("File already split");
                // List all files in directory and check if name ends with .nt.gz.part.*
                for (File file : ntTriples.getParentFile().listFiles()) {
                    if (file.getName().matches(ntTriples.getName() + ".part.*")) {
                        localTripleFileSubsets.add(file);
                    }
                }
            }

            // Double check if parts are not available...
            if (localTripleFileSubsets.size() == 0) {
                // Perform the conversion to nt.gz
                logger.info("Converting " + counter + " of " + hdtFiles.keySet().size() + " " + new File(hdtFile).getName() + " to NT file");
                HDT hdt = HDTManager.mapIndexedHDT("." + hdtFile);
                HDTGraph graph = new HDTGraph(hdt);
                Model model = ModelFactory.createModelForGraph(graph);
                GZIPOutputStream outputStream = new GZIPOutputStream(new FileOutputStream(ntTriples));
                RDFDataMgr.write(outputStream, model, Lang.NTRIPLES);
                outputStream.close();

                // Split file into 10 million reads per file?
                GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(ntTriples));
                BufferedReader buffered = new BufferedReader(new InputStreamReader(inputStream));
                int lineCounter = 0;
                String content;
                File localTripleFileSubset = new File(ntTriples + ".part." + lineCounter);

                localTripleFileSubsets.add(localTripleFileSubset);
                // First 10m stream
                PrintWriter localTripleOutputStream = new PrintWriter(new GZIPOutputStream(new FileOutputStream(localTripleFileSubset)));
                while ((content = buffered.readLine()) != null) {
                    // increment line counter
                    lineCounter++;
                    // Split in chunks of 10.000.000
                    if (lineCounter % 10000000 == 0) {
                        // Close the file stream
                        localTripleOutputStream.close();
                        // Make new file and stream with line counter
                        localTripleFileSubset = new File(ntTriples + ".part." + lineCounter);
                        logger.info("New part created " + localTripleFileSubset.getName());
                        // Add to list of output files
                        localTripleFileSubsets.add(localTripleFileSubset);
                        // The new output stream in gzip compression
                        localTripleOutputStream = new PrintWriter(new GZIPOutputStream(new FileOutputStream(localTripleFileSubset)));
                    }
                    // Writing the content line
                    localTripleOutputStream.println(content);
                }
                localTripleOutputStream.close();
            }

            // Iterate over all output subset files
            for (File tripleFileSubset : localTripleFileSubsets) {
                // Sent to database
                logger.info("Loading " + tripleFileSubset.getName() + " into " + commandOptions.graphdb);
                // Generate hash of file
                GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(tripleFileSubset));
                String sha256 = "sha256:"+org.apache.commons.codec.digest.DigestUtils.sha256Hex(inputStream);
                // If hash is present we can skip the loading process
                if (hashes.contains(sha256)) continue;

                hashes.add(sha256);

                // Load file into triple store
                inputStream = new GZIPInputStream(new FileInputStream(tripleFileSubset));
                loadZippedFile(inputStream, RDFFormat.NTRIPLES);
                // Add hash of file to triple store
                String statement = "<" + sha256 + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://m-unlock.nl/ontology/RDFDataSet> .";
                InputStream stream = new ByteArrayInputStream(statement.getBytes(StandardCharsets.UTF_8));
                repositoryConnection.add(stream, null, RDFFormat.NTRIPLES);
                stream.close();
            }
        }

        // Shutdown connection, repository and manager
        repositoryConnection.close();
        repository.shutDown();
        remoteRepositoryManager.shutDown();
    }

    private static void makeConnection(CommandOptions commandOptions) {
        String strServerUrl = commandOptions.graphdb;

        remoteRepositoryManager = RemoteRepositoryManager.getInstance(strServerUrl);
        remoteRepositoryManager.setUsernameAndPassword(commandOptions.username, commandOptions.password);
        remoteRepositoryManager.init();

        // Get the repository from repository manager, note the repository id
        // set in configuration .ttl file
        repository = remoteRepositoryManager.getRepository(commandOptions.project + "_" + commandOptions.investigation);

        // Open a connection to this repository
        repositoryConnection = repository.getConnection();
    }

    public static void loadZippedFile(InputStream in, RDFFormat format) throws IOException {
        NotifyingRepositoryConnectionWrapper con = new NotifyingRepositoryConnectionWrapper(repository, repository.getConnection());
        RepositoryConnectionListenerAdapter myListener =
                new RepositoryConnectionListenerAdapter() {
                    private long count = 0;

                    @Override
                    public void add(RepositoryConnection arg0, Resource arg1, IRI arg2,
                                    Value arg3, Resource... arg4) {
                        count++;
                        if (count % 100000 == 0)
                            logger.info("Add statement number " + count + "\t" + arg1 + " " + arg2 + " " + arg3);
                    }
                };
        con.addRepositoryConnectionListener(myListener);
        int attempt = 0;
        while (true) {
            attempt++;
            try {
                con.add(in, "", format);
                break;
            } catch (Exception e) {
                logger.error(e.getMessage());
                if (attempt > 5)
                    throw new IOException("After 5 attempts of loading the data it failed to continue. ");
            }
        }
    }

    public static void rdf(CommandOptions commandOptions) throws JargonException, IOException {
        Connection connection = new Connection();
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile("/" + connection.irodsAccount.getZone() + "/projects/" + commandOptions.project + "/" + commandOptions.investigation);
        logger.info("Searching for turtle files in " + irodsFile);

        makeConnection(commandOptions);

        for (File file : irodsFile.listFiles()) {
            if (file.isHidden()) continue;

            if (file.getName().endsWith(".ttl")) {
                downloadFile(connection, new File(file.getAbsolutePath()));
                FileInputStream inputStream = new FileInputStream("." + file.getAbsolutePath());
                repositoryConnection.add(inputStream, null, RDFFormat.TURTLE);
            }
        }
    }
}
