package nl.munlock.irods;

import nl.munlock.graphdb.options.CommandOptions;
import nl.munlock.graphdb.options.CommandOptionsHDT;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.*;
import org.irods.jargon.core.transfer.TransferStatusCallbackListener;
import org.rdfhdt.hdt.listener.ProgressListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Download {
    private static Logger logger = Logger.getLogger(Download.class);

    public static HashMap<String, String> findHDTFiles(CommandOptionsHDT commandOptions, String searchPath) throws GenQueryBuilderException, JargonException {
        logger.info("Searching for RDF files");

//        HashSet<String> loadedFiles = getLoadedFiles(commandOptions);

        Connection connection = new Connection();


        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.ttl");
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");
        // Find files in project and investigation
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, searchPath + "%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_DATA_CHECKSUM);
        // Order by column name
        queryBuilder.addOrderByGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, GenQueryOrderByField.OrderByType.ASC);
        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(100000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;

        try {
            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            TimeUnit.SECONDS.sleep(0);
        } catch (JargonException | InterruptedException | JargonQueryException e) {
            logger.error("Data retrieval failed");
            e.printStackTrace();
            return new HashMap<>();
        }

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashMap<String, String> paths = new HashMap<>();
        int count = irodsQueryResultSetResults.size();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            count = count - 1;
            if (count % 100 == 0) logger.info("Still " + count + " left");
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            String checksum = irodsQueryResultSetResult.getColumn(2);
            // Skipping files already in hdt file?
//            if (loadedFiles.contains(checksum)) continue;
            // Skipping files from directory provenance folder
            if (new File(path).getName().contains("directory")) continue;
            // Skipping hidden files
            if (new File(path).getName().startsWith(".")) continue;

            paths.put(path, checksum);
            // downloadFile(connection, new File(path));
        }
        return paths;
    }

    public static HashMap<String, String> findRDFFiles(String searchPath) throws GenQueryBuilderException, JargonException {
        logger.info("Searching for RDF files in " + searchPath);

        Connection connection = new Connection();

        // Check if folder truly exists!
        if (!connection.fileFactory.instanceIRODSFile(searchPath).exists()) {
            throw new JargonException("Folder does not exists");
        }

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.ttl");
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "%/hdt");
        // Find files in project and investigation
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, searchPath);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_DATA_CHECKSUM);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(100000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;
        try {
            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            TimeUnit.SECONDS.sleep(0);
        } catch (JargonException | InterruptedException | JargonQueryException e) {
            logger.error("Data retrieval failed");
            e.printStackTrace();
            return new HashMap<>();
        }

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashMap<String, String> paths = new HashMap<>();
        int count = irodsQueryResultSetResults.size();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            count = count - 1;
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            String checksum = irodsQueryResultSetResult.getColumn(2);
            paths.put(path, checksum);
        }
        return paths;
    }

    public static HashMap<String, String> findHDTFiles(CommandOptions commandOptions) throws GenQueryBuilderException, JargonException {
        logger.info("Searching for RDF files");

        HashSet<String> loadedFiles = getLoadedFiles(commandOptions);

        Connection connection = new Connection();

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.hdt");
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");
        // Find files in project and investigation
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, "/" + connection.irodsAccount.getZone() + "/projects/" + commandOptions.project + "/" + commandOptions.investigation+ "%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_DATA_CHECKSUM);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(100000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;
        try {
            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            TimeUnit.SECONDS.sleep(0);
        } catch (JargonException | InterruptedException | JargonQueryException e) {
            logger.error("Data retrieval failed");
            e.printStackTrace();
            return new HashMap<>();
        }

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashMap<String, String> paths = new HashMap<>();
        int count = irodsQueryResultSetResults.size();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            count = count - 1;
            if (count % 100 == 0) logger.info("Still " + count + " left");
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            String checksum = irodsQueryResultSetResult.getColumn(2);
            // Skipping files already in database
            if (loadedFiles.contains(checksum)) continue;
            // Skipping files from directory provenance folder
            if (new File(path).getName().contains("directory")) continue;
            // Skipping hidden files
            if (new File(path).getName().startsWith(".")) continue;

            paths.put(path, checksum);
            // downloadFile(connection, new File(path));
        }
        return paths;
    }

    public static HashSet<String> getObservationUnits() throws GenQueryBuilderException, JargonException, JargonQueryException, InterruptedException {
        System.err.println("Getting OU's");
        Connection connection = new Connection();

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_COLL_ATTR_NAME, QueryConditionOperators.EQUAL, "type");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_COLL_ATTR_VALUE, QueryConditionOperators.EQUAL, "ObservationUnit");
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");
        // Get folders
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(100000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;

        irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        TimeUnit.SECONDS.sleep(0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashSet<String> paths = new HashSet<>();
        int count = irodsQueryResultSetResults.size();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            count = count - 1;
            if (count % 100 == 0) logger.info("Still " + count + " left");
            String path = irodsQueryResultSetResult.getColumn(0);
            paths.add(path);
        }
        return paths;
    }

    private static HashSet<String> getLoadedFiles(CommandOptions commandOptions) {
        SPARQLRepository sparqlRepository = new SPARQLRepository(commandOptions.graphdb + "/repositories/" + commandOptions.project +"_" + commandOptions.investigation);
        sparqlRepository.setUsernameAndPassword(commandOptions.username, commandOptions.password);
        TupleQuery query = sparqlRepository.getConnection().prepareTupleQuery("PREFIX gbol: <http://gbol.life/0.1/> SELECT * WHERE { ?s rdf:type <http://m-unlock.nl/ontology/RDFDataSet> }");

        query.setMaxExecutionTime(10);
        HashSet<String> loadedFiles = new HashSet<>();
        for (BindingSet bindings : query.evaluate()) {

            loadedFiles.add(bindings.getValue("s").stringValue());
        }
        return loadedFiles;
    }

    /**
     * Downloads file to the current working directory
     *
     * @param connection the authentication object for iRODS
     * @param download   the file that needs to be downloasded
     * @throws JargonException
     */
    public static void downloadFile(Connection connection, File download) throws JargonException, FileNotFoundException {
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(download.getAbsolutePath());
        if (!irodsFile.exists()) {
            throw new JargonException("File " + irodsFile + " does not exist");
        }
        if (irodsFile.isHidden()) {
            logger.info("File is hidden");
            return;
        }
        logger.info("Downloading " + irodsFile);

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Create folder directory
        File directory = new File("." + download.getAbsolutePath().replaceAll(download.getName() + "$", ""));
        directory.mkdirs();

        File localFile = new File("." + download);

        if (localFile.exists()) {
            // Perform hash check!
            // Get local HASH
            LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
            SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
            ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
            // Get remote hash
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
            // If checksum is not the same remove local file
            if (!localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                logger.info("Removing local file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                logger.info("Does not match checksum of " + irodsFile.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                while (localFile.exists()) {
                    localFile.delete();
                }
            }
        }

        if (!localFile.exists()) {
            // Disables the logger for the transfer as it easily gives thousands of lines...
            // logger.info("Downloading " + localFile.getName() + " from " + irodsFile);
            dataTransferOperationsAO.getOperation(irodsFile, localFile, null, null);
        } else {
            logger.info("File already exists locally: " + localFile);
        }
    }
}
