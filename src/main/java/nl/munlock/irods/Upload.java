package nl.munlock.irods;

import nl.munlock.hdt.Create;
import org.apache.log4j.Logger;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;

import java.io.File;
import java.io.FileNotFoundException;

public class Upload {
    /**
     * Uploading a local file to iRODS
     *
     * @param connection the authentication object to access irods
     * @param localFile  a local file that will be uploaded to the irods location
     * @param remoteFile the path to the destination
     * @throws JargonException
     * @throws FileNotFoundException
     */
    static Logger logger = Logger.getLogger(Create.class);

    public static void uploadIrodsFile(Connection connection, File localFile, File remoteFile) throws JargonException, FileNotFoundException {
        logger.info("Uploading " + localFile + " to " + remoteFile);

        if (!localFile.exists()) {
            logger.error("Local file " + localFile + " does not exists... cancelling upload");
        }

        // Check if file is not an irods location
        IRODSFile irodsFileFinalDestination = connection.fileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());

        ChecksumValue remoteChecksumValue;
        ChecksumValue localChecksumValue;

        if (irodsFileFinalDestination.exists()) {

            // Get local HASH
            LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
            SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
            localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
            // Get remote hash
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFileFinalDestination);

            if (localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                logger.debug("Same checksum, not going to overwrite");
                return;
            } else {
                logger.info("Removing remote file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                logger.info("Does not match checksum of " + irodsFileFinalDestination.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                irodsFileFinalDestination.delete();
            }
        }

        if (!connection.fileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).exists()) {
            // Create collection in iRODS
            logger.info("Creating directory: " + remoteFile.getParentFile().getAbsolutePath());
            connection.fileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).mkdirs();
        }

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        IRODSFile destFile = connection.fileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());
        logger.debug(localFile + "\t" + remoteFile);
        dataTransferOperationsAO.putOperation(localFile, destFile, null, null);
    }
}
