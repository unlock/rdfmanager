package nl.munlock.irods;

import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.connection.SettableJargonProperties;
import org.irods.jargon.core.connection.SettableJargonPropertiesMBean;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;

import java.util.logging.Logger;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;

public class Connection {
    public final IRODSFileSystem irodsFileSystem;
    public final IRODSAccount irodsAccount;
    public final IRODSAccessObjectFactory accessObjectFactory;
    public final IRODSFileFactory fileFactory;
    private static final Logger log = Logger.getLogger(Connection.class.getName());

    public Connection() throws JargonException {
        // Initialize account object
        irodsAccount = IRODSAccount.instance(
                System.getenv("irodsHost"),
                Integer.parseInt(System.getenv("irodsPort")),
                System.getenv("irodsUserName"),
                System.getenv("irodsPassword"),
                "",
                System.getenv("irodsZone"),
                "");

        SettableJargonPropertiesMBean jargonProperties = new SettableJargonProperties();
        jargonProperties.setChecksumEncoding(ChecksumEncodingEnum.SHA256);

        // Disables SSL
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy;
        sslPolicy = CS_NEG_REQUIRE;

        clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        irodsFileSystem = IRODSFileSystem.instance();

        accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();

        fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);
    }
}
