package nl.munlock.hdt;

import nl.munlock.graphdb.options.CommandOptionsHDT;
import nl.munlock.irods.Connection;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.common.net.ParsedIRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.*;
import org.eclipse.rdf4j.model.util.URIUtil;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicParserSettings;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.exceptions.ParserException;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.listener.ProgressListener;
import org.rdfhdt.hdt.options.HDTSpecification;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;

import static nl.munlock.irods.Download.downloadFile;

public class Create {
    static Logger logger = Logger.getLogger(Create.class);

    public static void start(CommandOptionsHDT commandOptions, HashMap<String, String> rdfFiles) throws JargonException, IOException, ParserException {
        Connection connection = new Connection();
        HashSet<String> outputFiles = new HashSet<>();

        int counter = 0;
        for (String file : rdfFiles.keySet()) {
            IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(file);
            // Some skip checks
            if (irodsFile.isHidden()) continue;
            if (!irodsFile.exists()) continue;

            counter = counter + 1;
            if (counter % 1 == 0) {
                logger.info("Downloading " + counter + " files of " + rdfFiles.size() + " " + new File(file).getName());
            }
            String localFile = "." + file;
            String outputFile = "." + file.replaceAll(".ttl$", ".nt");

            if (new File(outputFile).exists() && new File(outputFile).length() > 10) {
                outputFiles.add(outputFile);
            } else {
                // Obtain file
                downloadFile(connection, new File(file));
                // Start RDF validation
                boolean validating = true;
                while (validating) {
                    try {
                        logger.info("Validating " + localFile);
                        InputStream inputStream = new FileInputStream(localFile);
                        FileOutputStream outputStream = new FileOutputStream(outputFile);

                        inputStream.close();
                        outputStream.close();

                        inputStream = new FileInputStream(localFile);
                        outputStream = new FileOutputStream(outputFile);
                        RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
                        RDFWriter rdfWriter = Rio.createWriter(RDFFormat.NTRIPLES, outputStream);
                        rdfParser.setRDFHandler(rdfWriter);
                        rdfParser.getParserConfig().set(BasicParserSettings.VERIFY_URI_SYNTAX, true);
                        rdfParser.parse(inputStream, "");

                        inputStream.close();
                        outputStream.close();

                        outputFiles.add(outputFile);
                        // Finished validation
                        validating = false;
                    } catch (Exception e) {
                        logger.warn("Failed to parse " + new File(localFile).getAbsolutePath());
                        logger.warn(e.getMessage());

                        if (e.getMessage().contains("Unexpected character")) {
                            String iri = e.getMessage().split(":", 2)[1].split("\\[line ")[0].trim();
                            // Move file to .error
                            File oldFile = new File(localFile);
                            File newFile = new File(localFile + ".error");
                            oldFile.renameTo(newFile);
                            iriCorrecter(newFile, oldFile, RDFFormat.TURTLE, iri);
                        }
                    }
                }
            }
        }
        String hdtFile = "./" + commandOptions.folder + "/hdt/" + new File(commandOptions.folder).getName() + ".hdt";
        concat(connection, outputFiles, hdtFile);
    }

    private static void concat(Connection connection, HashSet<String> outputFiles, String hdtFile) throws IOException, ParserException, JargonException {
        logger.info("Concatenating ntriple files");
        File ntriplesFile = new File("./" + hdtFile.replaceAll(".hdt$", ".nt"));
        ntriplesFile.getParentFile().mkdirs();
        PrintWriter ntriples = new PrintWriter(new FileOutputStream(ntriplesFile));
        for (String outputFile : outputFiles) {
            logger.info("Input files " + new File(outputFile).getName());
            BufferedReader br = new BufferedReader(new FileReader(new File(outputFile)));
            String line = br.readLine();
            while (line != null) {
                ntriples.println(line);
                line = br.readLine();
            }
            br.close();
        }
        ntriples.close();

        generateHDT(ntriplesFile, hdtFile);
        nl.munlock.irods.Upload.uploadIrodsFile(connection, new File(hdtFile), new File(hdtFile.replaceFirst("^\\.", "")));
        nl.munlock.irods.Upload.uploadIrodsFile(connection, new File(hdtFile + ".index.v1-1"), new File(hdtFile.replaceFirst("^\\.", "") + ".index.v1-1"));
        // Remove NT file
        ntriplesFile.delete();
    }

    private static void iriCorrecter(File inputFile, File outputFile, RDFFormat rdfFormat, String iri) throws IOException {
        iri = iri.trim();
        String iriFix = ParsedIRI.create(iri).toASCIIString();

        FileReader fr=new FileReader(inputFile);   //reads the file
        BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
        StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters
        String line;
        PrintWriter printWriter = new PrintWriter(outputFile);
        while((line=br.readLine())!=null) {
            if (line.indexOf(iri) > 0) {
                line = line.replace(iri, iriFix);
            }
            printWriter.println(line);
        }
        printWriter.close();
        br.close();
        fr.close();
    }

    public static void generateHDT(File rdfInput, String hdtFile) throws IOException, ParserException {
        logger.info("Generating HDT file");
        // Configuration variables
        String baseURI = "http://gbol.life/0.1/";
        String inputType = "ntriples";
        String hdtOutput = hdtFile;

        // Create HDT from RDF file
        ProgressListener progressListener = (level, message) -> logger.info(level + " - " + message);

        HDT hdt = HDTManager.generateHDT(
                rdfInput.getAbsolutePath(),         // Input RDF File
                baseURI,          // Base URI
                RDFNotation.parse(inputType), // Input Type
                new HDTSpecification(),   // HDT Options
                progressListener              // Progress Listener
        );

        // OPTIONAL: Add additional domain-specific properties to the header:
        //Header header = hdt.getHeader();
        //header.insert("myResource1", "property" , "value");

        // Save generated HDT to a file
        hdt.saveToHDT(hdtOutput, null);
        hdt.close();

        // Generate index?
        HDTManager.loadIndexedHDT(hdtOutput, progressListener);

        logger.info("HDT file generated at " + hdtOutput);
    }
}