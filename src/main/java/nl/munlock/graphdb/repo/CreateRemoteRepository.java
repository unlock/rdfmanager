package nl.munlock.graphdb.repo;


import nl.munlock.graphdb.options.CommandOptions;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.config.RepositoryConfigSchema;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateRemoteRepository {
    private static Logger logger = Logger.getLogger(CreateRemoteRepository.class);

    public static void main(CommandOptions commandOptions) throws IOException {
        logger.info("Creating repository");
        Path path = Paths.get(".").toAbsolutePath().normalize();
        String strRepositoryConfig = path.toFile().getAbsolutePath() + "/src/main/resources/graphdb/repo-config.ttl";
        String strServerUrl = "https://www.systemsbiology.nl/whatever/";

        // Instantiate a local repository manager and initialize it
        RemoteRepositoryManager remoteRepositoryManager = RemoteRepositoryManager.getInstance(strServerUrl);
        remoteRepositoryManager.setUsernameAndPassword("admin", "root");
        remoteRepositoryManager.initialize();
        remoteRepositoryManager.getAllRepositories();

        // Instantiate a repository graph model
        TreeModel graph = new TreeModel();

        // Read repository configuration file
        InputStream config = new FileInputStream(strRepositoryConfig);
        RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
        rdfParser.setRDFHandler(new StatementCollector(graph));
        rdfParser.parse(config, RepositoryConfigSchema.NAMESPACE);
        config.close();

        // Retrieve the repository node as a resource
        Resource repositoryNode = Models.subject(graph.filter(null, RDF.TYPE, RepositoryConfigSchema.REPOSITORY)).orElseThrow(() -> new RuntimeException(
                        "Oops, no <http://www.openrdf.org/config/repository#> subject found!"));


        // Create a repository configuration object and add it to the repositoryManager
        RepositoryConfig repositoryConfig = RepositoryConfig.create(graph, repositoryNode);
        repositoryConfig.setID(commandOptions.project +"_" + commandOptions.investigation);
        repositoryConfig.setTitle("Project " + commandOptions.project + " investigating " + commandOptions.investigation);
        remoteRepositoryManager.addRepositoryConfig(repositoryConfig);
    }
}