package nl.munlock.graphdb.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

public class CommandOptionsHDT {

    @Parameter(names = {"--help", "-h", "-help"})
    public boolean help = false;

    @Parameter(names = {"-folder"}, description = "Folder to be converted to HDT (recursively)", required = true)
    public String folder;

    @Parameter(names = {"-debug"}, description = "Enable debug mode")
    public boolean debug;

    @Parameter(names = {"-hdt"}, description = "Running in HDT mode", required = true)
    public boolean hdt;

//    @Parameter(names = {"-force"}, description = "Force rebuilding of HDT files")
//    public boolean force;

    public String[] args;

    public CommandOptionsHDT(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);
            if(this.help)
                throw new ParameterException("Help required");
            this.args = args;
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help)
                exitCode = 0;

            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.out.println("Please set the following system variables irodsHost, irodsPort, irodsUserName, irodsPassword, irodsZone");
            System.exit(exitCode);
        }
    }
}
