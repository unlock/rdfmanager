package nl.munlock.graphdb.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

public class CommandOptions {

    @Parameter(names = {"--help", "-h", "-help"})
    public boolean help = false;

    @Parameter(names = {"-project"}, description = "Project folder name", required = true)
    public String project;

    @Parameter(names = {"-investigation"}, description = "Investigation folder name", required = true)
    public String investigation;

    @Parameter(names = {"-debug"}, description = "Enable debug mode")
    public boolean debug;

    public String[] args;

    @Parameter(names = {"-graphdb"}, description = "GraphDB REST endpoint e.g. (http://192.0.2.1:7200/rest/repositories)")
    public String graphdb;

    @Parameter(names = {"-password"}, description = "GraphDB password")
    public String password;

    @Parameter(names = {"-username"}, description = "GraphDB username")
    public String username;

    @Parameter(names = {"-base"}, description = "Base URI")
    public String base = "http://m-unlock.nl/ontology/";

    public CommandOptions(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);
            if(this.help)
                throw new ParameterException("Help required");
            this.args = args;
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help)
                exitCode = 0;

            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.out.println("Please set the following system variables irodsHost, irodsPort, irodsUserName, irodsPassword, irodsZone");
            System.exit(exitCode);
        }
    }
}
