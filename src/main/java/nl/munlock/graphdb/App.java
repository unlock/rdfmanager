package nl.munlock.graphdb;

import nl.munlock.graphdb.options.CommandOptions;
import nl.munlock.graphdb.options.CommandOptionsHDT;
import nl.munlock.graphdb.repo.CreateRemoteRepository;
import nl.munlock.irods.Download;
import nl.munlock.irods.Load;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.rdfhdt.hdt.exceptions.ParserException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Project initializer which converts a metadata excel sheet obtained from
 *
 */
public class App {
    static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(App.class);

    public static void main(String[] args) throws IOException, GenQueryBuilderException, JargonException, ParserException {

        List arguments = Arrays.asList(args);

        // Check if graphdb is in args
        if (arguments.contains("-graphdb")) {
            logger.info("Graphdb parsing");
            CommandOptions commandOptions = new CommandOptions(args);
            // Create repository
            CreateRemoteRepository.main(commandOptions);
            // Change to HDT...
            HashMap<String, String> hdtFiles = Download.findHDTFiles(commandOptions);
            Load.hdt(commandOptions, hdtFiles);
            // Load investigation file
            Load.rdf(commandOptions);
        } else if (arguments.contains("-hdt")) {
            logger.info("HDT parsing");
            // Check if hdt is in args
            CommandOptionsHDT commandOptions = new CommandOptionsHDT(args);
            HashMap<String, String> rdfFiles = Download.findRDFFiles(commandOptions.folder + "%");
            if (rdfFiles.size() > 0) {
                nl.munlock.hdt.Create.start(commandOptions, rdfFiles);
            } else {
                throw new IOException("No TTL files detected");
            }
        } else {
            logger.info("Please use -hdt or -graphdb");
        }
    }
}
