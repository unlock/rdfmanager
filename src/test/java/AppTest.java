import nl.munlock.graphdb.App;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.junit.Test;
import org.rdfhdt.hdt.exceptions.ParserException;

import java.io.File;
import java.io.IOException;

import static nl.munlock.hdt.Create.generateHDT;

public class AppTest {

    @Test
    public void prokopis() throws IOException, GenQueryBuilderException, JargonException, ParserException {
        String[] args = {
                "-project", "P_E2BN",
                "-investigation", "I_PRED",
                "-graphdb", "https://www.systemsbiology.nl/whatever/"};
        App.main(args);
    }

    @Test
    public void kers() throws IOException, GenQueryBuilderException, JargonException, ParserException {
        String[] args = {
                "-project", "P_FIRM-Project",
                "-investigation", "I_FIRM-Broilers",
                "-graphdb", "https://www.systemsbiology.nl/whatever/",
                "-username", "admin",
                "-password", "root"
        };
        App.main(args);
    }

    @Test
    public void testHDT() throws IOException, GenQueryBuilderException, JargonException, ParserException {
//        String folder = "/unlock/projects/P_MIB-Amplicon/I_Poultry_16S_MIB/S_FIRM2/O_1.15.BCA.1";
//        String folder = "/unlock/projects/P_MIB-Amplicon/I_Mocks/S_Mocks_3_4/O_MOCK_3";
//        String folder = "/unlock/projects/P_MIB-Amplicon/I_Poultry_16S_MIB/S_EndLESS/O_strooisel4";
        String[] folders = {"/unlock/projects/P_E2BN/I_PRED/S_PREDIMED_Microbiota/O_Mock3/","/unlock/projects/P_E2BN/I_PRED/S_PREDIMED_Microbiota/O_Mock4/"};
        for (String folder : folders) {
            System.err.println(folder);
            String[] args = {
                    "-folder", folder,
                    "-hdt",
                    "-debug"
            };
            App.main(args);
        }
    }

    @Test
    public void testProgress() throws IOException, ParserException {
        generateHDT(new File("./unlock/projects/P_MIB-Amplicon/I_Poultry_16S_MIB/S_Cargill_study_Hugo/S_Cargill_study_Hugo.nt"), "./unlock/projects/P_MIB-Amplicon/I_Poultry_16S_MIB/S_Cargill_study_Hugo/S_Cargill_study_Hugo.hdt");
    }

    @Test
    public void mibmock(){
        String[] args = {
                "-project", "P_MIB-Amplicon",
                "-investigation", "I_Poultry_16S_MIB",
                "-graphdb", "https://www.systemsbiology.nl/whatever/",
                "-username", "admin",
                "-password", "root"
        };
        while (1==1) {
            try {
                App.main(args);
                return;
            } catch (IOException | GenQueryBuilderException | JargonException | ParserException e) {
                e.printStackTrace();
            }
        }
    }
}
